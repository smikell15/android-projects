package mikell.appoice.com.trendingnow;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class LocationUpdateService extends Service {

    public static String BROADCAST_ACTION = "com.appoice.mikell.trendingnow.BROADCAST";

    private LocationManager _locationManager;
    private boolean _gpsEnabled,_networkEnabled;
    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        _locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        _gpsEnabled = _locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        _networkEnabled = _locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

    }

    public void onStart(Intent intent, int startId) {

        Log.e("onStart","onStart");

        if( _gpsEnabled ) {
            _locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    1000, 0, locationListener);
        } else if ( _networkEnabled ){
            _locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    1000, 0, locationListener);
        }

    };




    LocationListener locationListener = new LocationListener() {
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onLocationChanged(final Location location) {
            final Location currentLocation = location;

            Log.e("onLocationChanged", " " + currentLocation);
            _locationManager.removeUpdates(locationListener);

            Intent intent = new Intent();
            intent.setAction(BROADCAST_ACTION);
            Bundle b = new Bundle();
            b.putDouble("currentLatitude", currentLocation.getLatitude());
            b.putDouble("currentLongitude", currentLocation.getLongitude());
            intent.putExtras(b);
            sendBroadcast(intent);
        }

    };
}
